import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid w-100">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse w-100" id="navbarSupportedContent">
          <div className='row w-100 justify-content-between'>
          <ul className="navbar-nav col-12 me-auto mb-2 mb-lg-0  w-100 justify-content-between">
              <li className="nav-item">
                <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
              </li>
                  <li className="nav-item">
                    <NavLink className="nav-link" to="/manufacturers/form">Create a Manufacturer</NavLink>
                  </li>
                      <li className="nav-item">
                        <NavLink className="nav-link" to="/models">Models</NavLink>
                      </li>
                        <li className="nav-item">
                          <NavLink className="nav-link" to="/models/form">Create a model</NavLink>
                        </li>
                          <li className="nav-item">
                            <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
                          </li>
                        <li className="nav-item">
                          <NavLink className="nav-link" to="/automobiles/form">Create an Automobile</NavLink>
                        </li>
                    <li className="nav-item">
                      <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
                    </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/salespeople/form">Add a Salesperson</NavLink>
                </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>
          </ul>
          <ul className="navbar-nav col-12 me-auto mb-2 mb-lg-0  w-100 justify-content-between">
              <li className="nav-item">
                  <NavLink className="nav-link" to="/customers/form">Add a Customer</NavLink>
              </li>
                  <li className="nav-item">
                    <NavLink className="nav-link" to="/sales">Sales</NavLink>
                  </li>
                      <li className="nav-item">
                        <NavLink className="nav-link" to="/sales/form">Add a Sales</NavLink>
                      </li>
                          <li className="nav-item">
                            <NavLink className="nav-link" to="/sales/history">Sales History</NavLink>
                          </li>
                              <li className="nav-item">
                                <NavLink className="nav-link" to="/technician">Technicians</NavLink>
                              </li>
                          <li className="nav-item">
                            <NavLink className="nav-link" to="/technician/form">Add a Technician</NavLink>
                          </li>
                      <li className="nav-item">
                        <NavLink className="nav-link" to="/service">Service Appointments</NavLink>
                      </li>
                  <li className="nav-item">
                    <NavLink className="nav-link" to="/service/form">Create a Service Appointment</NavLink>
                  </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/service/history">Service History</NavLink>
              </li>
          </ul>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
