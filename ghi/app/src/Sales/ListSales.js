import {useState, useEffect} from 'react'

function ListSales() {
    const [sales, setSale] = useState([]);


    async function loadSale() {
        const response = await fetch ("http://localhost:8090/api/sales/")
        const data = await response.json();
        setSale(data.sales)
    }

    useEffect(() => {
        loadSale();
    }, []);

    
return (
    <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
            {sales.map(sale => {
            return (
            <tr key={sale.id}>
                <td>{ sale.salesperson.employee_id }</td>
                <td>{ sale.salesperson.first_name }</td>
                <td>{ sale.customer.first_name }</td>
                <td>{ sale.automobile.vin }</td>
                <td> ${ sale.price }</td>
            </tr>
        );
    })}
            </tbody>
        </table>
    </div>
)

}

export default ListSales;
