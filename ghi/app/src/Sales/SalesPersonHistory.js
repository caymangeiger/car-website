import {useState, useEffect} from 'react'

function SalespersonHistory() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState("");

    async function loadSalespersonHistory() {
        const response = await fetch ("http://localhost:8090/api/sales/")
        const data = await response.json();
        setSales(data.sales)
    }
    async function loadSalespeople() {
        const response = await fetch ("http://localhost:8090/api/salespeople/")
        const data = await response.json();
        setSalespeople(data.salespersons)
    }

    useEffect(() => {
        loadSalespersonHistory();
        loadSalespeople();
    }, []);

    function handleSalespersonChange(e) {
        setSalesperson(e.target.value)
    }


    useEffect(() => {
    async function filterSales() {
        if (salesperson) {
            const response = await fetch ("http://localhost:8090/api/sales")
            const data = await response.json();
            setSales(data.sales.filter(sale => sale.salesperson.employee_id === salesperson))
        }
    }
    filterSales();
    }, [salesperson]);


return (
    <div>
        <div className="mb-3">
            <select value={salesperson} onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
            <option className="text-center">Choose a Salesperson</option>
            {salespeople.map(salesperson => {
                return (
                <option className="text-center" key={salesperson.employee_id} value={salesperson.employee_id} >
                {salesperson.first_name} {salesperson.last_name}
                </option>
                );
            })}
            </select>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Prices</th>
                </tr>
            </thead>
            <tbody>
            {sales.map(sale => {
            return (
            <tr key={sale.id}>
                <td>{ sale.salesperson.first_name } {sale.salesperson.last_name} </td>
                <td>{ sale.customer.first_name } {sale.customer.last_name} </td>
                <td>{ sale.automobile.vin }</td>
                <td> ${ sale.price }</td>
            </tr>
        );
    })}
            </tbody>
        </table>
    </div>
)
}

export default SalespersonHistory;
