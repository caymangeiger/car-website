import {useState, useEffect} from 'react';

function ServiceList() {
    const [appointments, setAppointments] = useState([]);
    const [autoMobiles, setAutoMobiles] = useState([]);
    const filterApptointments = appointments.filter(appt => { return appt.status === "created"})

    async function loadAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const data = await response.json();
        setAppointments(data.appointments);
    }


    async function loadAutoMobiles() {
        const response = await fetch('http://localhost:8080/api/automobiles/');
        const data = await response.json();
        setAutoMobiles(data.automobiles);
    }


    useEffect(() => {
        loadAutoMobiles();
        loadAppointments();
    }, []);


    async function handleOnCancel(e) {
        const request = await fetch (`http://localhost:8080/api/appointments/${e.target.id}/cancel/`, {
            method: "PUT"
        });
        if (request.ok){
            loadAppointments()
        } else {
        }
    }

    async function handleOnFinish(e) {
        const request = await fetch (`http://localhost:8080/api/appointments/${e.target.id}/finish/`, {
            method: "PUT"
        });
        const resp = await request.json()
        if (resp.status === "finished"){
            loadAppointments()
        } else {
        }
    }


return (
    <div className="mt-3">
        <h1>Service Appointments</h1>
    <table className="table table-striped">
        <thead className='mt-5'>
        <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
        </tr>
        </thead>
        <tbody>
        {filterApptointments.map(appointment => {
            return (
            <tr key={appointment.id}>
                <td>{ appointment.vin }</td>
                <td>{autoMobiles.some(auto => auto.vin === appointment.vin) ? "Yes":"No" }</td>
                <td>{ appointment.customer}</td>
                <td>{ appointment.date }</td>
                <td>{ appointment.time }</td>
                <td>{ appointment.technician.first_name }</td>
                <td>{ appointment.reason}</td>
                <td>
                    <button onClick={handleOnCancel} id={appointment.id}className="btn btn-danger btn-sm">Cancel</button>
                    <button onClick={handleOnFinish} id={appointment.id}className="btn btn-success btn-sm">Finish</button>
                </td>
            </tr>
            );
        })}
        </tbody>
    </table>
</div>
);
}
export default ServiceList;
