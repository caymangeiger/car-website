import React, { useState, useEffect } from 'react';

function ServiceForm() {
    const [technicians, setTechnicians] = useState([])
    const [date, setDate] = useState("")
    const [time, setTime] = useState("")
    const [formData, setFormData] = useState({
        date_time: '',
        reason: '',
        vin: '',
        customer: '',
        technician: '',
    });


    async function loadTechnicians() {
        const response = await fetch('http://localhost:8080/api/technicians/');
        const data = await response.json();
        setTechnicians(data.technicians);
    }
    useEffect(() => {
        loadTechnicians();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const serviceUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(serviceUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                reason: '',
                vin: '',
                customer: '',
                technician: '',
            });
            setDate('')
            setTime('')
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            date_time: date + ":" + time,
            [inputName]: value
        });
    }

    const handleDateChange = (e) => {
        setDate(e.target.value)
    }

    const handleTimeChange = (e) => {
        setTime(e.target.value)
    }

    
    return (
        <div className="row">
            <div className="offset-3 col-5">
                <div className="shadow p-4 mt-4">
                    <div className="col-12 mb-3 mt-0 pt-0" >
                    <h1>Create a service appointment</h1>
                    </div>
                    <form onSubmit={handleSubmit} id="create-service-form">
                        <div className="col-12  mt-3">
                            <label htmlFor="vin">Automobile VIN</label>
                            <input className="form-control" onChange={handleFormChange} value={formData.vin} required type="text" name="vin" id="vin"/>
                        </div>
                        <div className="col-12  mt-3">
                            <label htmlFor="customer">Customer</label>
                            <input className="form-control" onChange={handleFormChange} value={formData.customer} required type="text" name="customer" id="customer"/>
                        </div>
                        <div className="col-12  mt-3">
                            <label htmlFor="date">Date</label>
                            <input className="form-control" onChange={handleDateChange} value={date} required type="date" name="date" id="date"/>
                        </div>
                        <div className="col-12  mt-3">
                            <label htmlFor="time">Time</label>
                            <input className="form-control" onChange={handleTimeChange} value={time} required type="time" name="time" id="time"/>
                        </div>
                        <div className="col-12  mt-3">
                            <label htmlFor="date">Technician</label>
                            <select value={formData.technician} onChange={handleFormChange} required name="technician" id="technician" className="form-select">
                            <option>Choose a technician...</option>
                            {technicians.map(technician => {
                                return (
                                <option key={technician.id} value={technician.id} >
                                {technician.first_name}
                                </option>
                                );
                            })}
                            </select>
                        </div>
                        <div className="col-12  mt-3">
                            <label htmlFor="reason">Reason</label>
                            <input placeholder="Reason" className="form-control" onChange={handleFormChange} value={formData.reason} required type="text" name="reason" id="reason"/>
                        </div>
                        <button className="btn btn-primary mt-4">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default ServiceForm;
