import React, { useState, useEffect } from 'react';

function AutomobileForm() {
    const [models, setModels] = useState([])
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });


    async function loadModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        const data = await response.json();
        setModels(data.models);
    }
    useEffect(() => {
        loadModels();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const automobileUrl = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(automobileUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            });
        }

    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    return (
        <div className="row">
            <div className="offset-3 col-5">
                <div className="shadow p-4 mt-4">
                    <div className="col-12 mb-3 mt-0 pt-0" >
                    <h1>Add an automobile to inventory</h1>
                    </div>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="col-12">
                            <input placeholder="Color..." className="form-control" onChange={handleFormChange} value={formData.color} required type="text" name="color" id="color"/>
                        </div>
                        <div className="col-12 mt-2">
                            <input placeholder="Year..." pattern="\d{4}" className="form-control" onChange={handleFormChange} value={formData.year} required type="text" name="year" id="year"/>
                        </div>
                        <div className="col-12 mt-2">
                            <input placeholder="VIN..." className="form-control" onChange={handleFormChange} value={formData.vin} required type="text" name="vin" id="vin"/>
                        </div>
                        <div className="col-12 mt-2">
                            <select value={formData.model_id} onChange={handleFormChange} required name="model_id" id="model_id" className="form-select">
                            <option>Choose a model...</option>
                            {models.map(model => {
                                return (
                                <option key={model.id} value={model.id} >
                                {model.name}
                                </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary mt-3">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default AutomobileForm;
