import {useState, useEffect} from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    async function loadManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        const data = await response.json();
        setManufacturers(data.manufacturers);
    }
    useEffect(() => {
        loadManufacturers();
    }, []);


return (
    <div className="mt-3">
        <h1>Manufacturers</h1>
    <table className="table table-striped">
        <thead className='mt-5'>
        <tr>
            <th>Name</th>
        </tr>
        </thead>
        <tbody>
        {manufacturers.map(manufacturer => {
            return (
            <tr key={manufacturer.id}>
                <td>{ manufacturer.name }</td>
            </tr>
            );
        })}
        </tbody>
    </table>
</div>
);
}
export default ManufacturerList;
