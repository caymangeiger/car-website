import React, { useState, useEffect } from 'react';

function ModelForm() {
    const [manufacturers, setManufacturers] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    });


    async function loadManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        const data = await response.json();
        setManufacturers(data.manufacturers);
    }
    useEffect(() => {
        loadManufacturers();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const modelUrl = 'http://localhost:8100/api/models/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            });
        }

    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    
    return (
        <div className="row">
            <div className="offset-3 col-5">
                <div className="shadow p-4 mt-4">
                    <div className="col-12 mb-3 mt-0 pt-0" >
                    <h1>Create a vehicle model</h1>
                    </div>
                    <form onSubmit={handleSubmit} id="create-model-form">
                        <div className="col-12">
                            <input placeholder="Model name..." className="form-control" onChange={handleFormChange} value={formData.name} required type="text" name="name" id="name"/>
                        </div>
                        <div className="col-12 mt-2">
                            <input placeholder="Picture URL..." className="form-control" onChange={handleFormChange} value={formData.picture_url} required type="url" name="picture_url" id="picture_url"/>
                        </div>
                        <div className="col-12 mt-2">
                            <select value={formData.manufacturer_id} onChange={handleFormChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                            <option>Choose a manufacturer...</option>
                            {manufacturers.map(manufacturer => {
                                return (
                                <option key={manufacturer.id} value={manufacturer.id} >
                                {manufacturer.name}
                                </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary mt-3">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default ModelForm;
