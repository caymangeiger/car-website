from django.urls import path
from .views import listCustomer, customerDetail, listSales, salesDetail, listSalesperson, salespersonDetail

urlpatterns = [
    path("sales/", listSales, name="listCustomer"),
    path("sales/<int:pk>/", salesDetail, name="customerDetail"),
    path("customers/", listCustomer, name="listCustomer"),
    path("customers/<int:pk>/", customerDetail, name="customerDetail"),
    path("salespeople/", listSalesperson, name="listCustomer"),
    path("salespeople/<int:pk>/", salespersonDetail, name="customerDetail")
]
