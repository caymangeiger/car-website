from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .encoders import (
    TechnicianEncoder,
    AppointmentEncoder,
    AutomobileVOEncoder,
)
import json
from .models import Appointment, Technician, AutomobileVO


@require_http_methods(["GET", "POST"])
def appointmentList(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technicianID = content["technician"]
            technician = Technician.objects.get(id=technicianID)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def appointmentDetail(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment Does Not Exist"},
                status=400,
            )
    else:
        try:
            appointment = Appointment.objects.get(id=pk)
            count, _ = Appointment.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment Does Not Exist"},
                status=400,
            )


@require_http_methods(["PUT"])
def appointmentCancel(request, pk):
    if request.method == "PUT":
        try:
            status = Appointment.objects.get(id=pk)
            canceled = "canceled"
            status = canceled
            Appointment.objects.filter(id=pk).update(status=status)
            appointmentStatus = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointmentStatus,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment Does Not Exist"},
                status=400,
            )


@require_http_methods(["PUT"])
def appointmentFinish(request, pk):
    if request.method == "PUT":
        try:
            status = Appointment.objects.get(id=pk)
            finished = "finished"
            status = finished
            Appointment.objects.filter(id=pk).update(status=status)
            appointmentStatus = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointmentStatus,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment Does Not Exist"},
                status=400,
            )


@require_http_methods(["GET", "POST"])
def technicianList(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technicianID = content["employee_id"]
            technician = Technician.objects.get(employee_id=technicianID)
            content["employee_id"] = technician
        except Technician.DoesNotExist:
            pass
        else:
            return JsonResponse(
                {"message": "Technician ID already in use"},
                status=400,
            )
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def technicianDetail(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician Does Not Exist"},
                status=400,
            )
    else:
        try:
            technician = Technician.objects.get(id=pk)
            count, _ = Technician.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician Does Not Exist"},
                status=400,
            )


@require_http_methods(["GET"])
def autoMobileList(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
            safe=False
        )
